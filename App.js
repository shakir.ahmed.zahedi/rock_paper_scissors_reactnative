import React, {useEffect, useState, createContext} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, TextInput} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Home from "./components/Home";
import JoinStack from "./components/JoinStack";
import StartStack from "./components/StartStack";
import Header from "./components/Header";
import TokenContext from "./context/TokenContext";




export default function App() {
    const [token, setToken] = React.useState("");
    useEffect(() => {
        fetch('http://192.168.0.7:8086/auth/token')
            .then((response) => response.text())
            .then(text => {
                setToken(text);
            })
            .catch((error) => console.error(error))
    }, [])
    console.log("Token:" + token);
    const Drawer = createDrawerNavigator();

    return (
        <TokenContext.Provider value={token}>
            <NavigationContainer>
                <Drawer.Navigator>
                    <Drawer.Screen name="Home"
                                   component={Home}

                    />
                    <Drawer.Screen name="Join Game" component={JoinStack}
                                   options={({navigation}) =>
                                       ({
                                           headerTitle: () => <Header navigation={navigation}/>
                                       })}
                    />
                    <Drawer.Screen name="Start Game" component={StartStack}/>
                </Drawer.Navigator>
            </NavigationContainer>
        </TokenContext.Provider>
        /*<NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Home" component={Home} />

            <Stack.Screen name="Form" component={Form} />
            <Stack.Screen name="GameList" component={GameList} />

            <Stack.Screen name="Game" component={Game} />
          </Stack.Navigator>
        </NavigationContainer>*/
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 30
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
    },
    home: {
        margin: 10,
        width: '100%',
        height: 600,
        padding: 20
    },
    cardHeader: {
        width: '100%',
        height: 50,
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',


    }
});

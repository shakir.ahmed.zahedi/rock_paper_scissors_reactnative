import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Game from "./Game";
import Form from "./Form";
import GameList from "./GameList";
import StartNewGame from "./StartNewGame";

const Stack = createStackNavigator();
export default function StartStack() {
    return (

            <Stack.Navigator>
                <Stack.Screen name="New Game" component={StartNewGame}/>
                <Stack.Screen name="Game" component={Game}/>
            </Stack.Navigator>

    )
}
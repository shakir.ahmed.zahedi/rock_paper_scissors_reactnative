
import React, {useEffect,useState} from 'react';
import {StyleSheet, Text, View, Image, SafeAreaView, TouchableOpacity, Button} from 'react-native';
import logo from '../assets/logo.png';
import { Ionicons } from '@expo/vector-icons';

export default function Header({navigation}) {
    return (
        <View style={styles.container}>
            <Image source={logo} style={styles.img} />
            <View style={styles.icon}>
                <TouchableOpacity onPress={()=>navigation.openDrawer()}>
                    <Ionicons name="menu" size={30} color="red" />
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        //paddingTop:10,
        backgroundColor:'white',
        justifyContent: 'space-around',
        width: '100%'
    },
    img:{
        width: 250,
        height: 80,
        marginLeft:10,
        marginTop:10,


    },
    icon:{
        backgroundColor: '#ffdc6a',
        borderRadius:10,
        borderWidth:1,
        borderColor:'red',
        padding:2,
        marginLeft:5,
        marginTop:30,
        marginBottom:10

    }
});

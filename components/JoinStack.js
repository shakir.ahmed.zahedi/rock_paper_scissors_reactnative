import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Form from "./Form";
import GameList from "./GameList";
import Game from "./Game";
import Header from "./Header";
import JoinGame from "./JoinGame";


export default function JoinStack() {
    const Stack = createStackNavigator();
    return (

            <Stack.Navigator>
                <Stack.Screen name="JoinGame" component={JoinGame}
                             /* options={({ navigation })=>
                                  ({
                                      //headerTitle: () => <Header navigation={navigation} />,
                                      headerStyle: {
                                          backgroundColor: 'grey',
                                          height:150
                                      }
                                  })}*/
                />
                <Stack.Screen name="GameList" component={GameList}
                             /* options={({ navigation })=>
                                  ({
                                     // headerTitle: () => <Header navigation={navigation}/>,
                                      headerStyle: {
                                          backgroundColor: 'grey',
                                          height:150
                                      }

                                  })}*/
                />

                <Stack.Screen name="Game" component={Game}
                             /* options={({ navigation })=>
                                  ({
                                      //headerTitle: () => <Header navigation={navigation}/>,
                                      headerStyle: {
                                          backgroundColor: 'grey',
                                          height:150
                                      }
                                  })}*/
                />
            </Stack.Navigator>
    )
}
const styles = StyleSheet.create({
    ami:{

     margin:0
    }


});

import React from 'react';
import {StyleSheet, Text, View, Image, SafeAreaView, TouchableOpacity, Button} from 'react-native';
import Header from "./Header";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Try from "./Try";


export default function Home({ navigation }) {
    return (
        <View style={styles.container}>
            <View>
                <Header navigation={navigation}/>
            </View>
            <View  style={styles.body}>
                <Text style={styles.bodyText}>Welcome</Text>
               <Text style={styles.bodyText}> Lets Play....</Text>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop:30,
        backgroundColor:'gray'
    },
    body: {
        flex: 2,
        justifyContent: 'center',
        alignItems:'center'
    },
    bodyText: {
        fontWeight: 'bold',
        fontSize: 30,
        color: 'rgb(223, 31, 127)'
    }

});

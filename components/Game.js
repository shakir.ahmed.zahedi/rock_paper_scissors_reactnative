import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View, Image, SafeAreaView, Button, TouchableOpacity} from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import Card from "./Card";
import Home from "./Home";
import Api from "../api/Api";
import TokenContext from "../context/TokenContext";

export default function Game( {route,navigation}) {
    const [state,setState]= useState("Make Your Move");
    const [game,setGame]= useState("");
    const [opponentMove,setOpponentMove]= useState("");
    const [opponentName,setOpponentName]= useState("");
    const [move,setMove]= useState("");
    const [name,setName]= useState("");
    const [result,setResult]= useState("");
    const [ready,setReady]= useState("Not Ready");
    const [intervalId, setIntervalId]= useState();
   // const { id, otherParam } = route.params;
    const token = useContext(TokenContext);
    const [count, setCount]= useState('false');
    const assignMove=(move)=>{
        if(count==='false')
        Api.makeMove(token,move).then(res=>{
            setCount('true');
            setState("YOUR MOve Is register");
            console.log(res)})
        else
        alert("You already chose your option");
    }
    const status=()=>{
        Api.getGameStatus(token).then(res=>{
            //setName(res.name);
            //setMove(res.move);
            setResult(res.game);
            //setOpponentMove(res.opponentMove);
            //setOpponentName(res.opponentName);
            console.log(res);
    })}
   // const stop=()=>setInterval(status, 3000);
    useEffect(() => {
        if(result==='WIN' ||result==='LOSE'||result==='DRAW') {
            Api.getGameStatus(token).then(res => {
                setName(res.name);
                setMove(res.move);
                setResult(res.game);
                setOpponentMove(res.opponentMove);
                setOpponentName(res.opponentName);
                console.log("ThankYou");
            })
            //status();
            console.log("ThankYou");
            clearInterval(intervalId);
        }


        else{
            const id = setInterval(status, 3000);
            setIntervalId(id);
        }
        return () => clearInterval(intervalId);
        //setInterval(status,3000);

    }, [result,navigation]);

    return (
        <View >
            <Card >
                <View style={styles.cardHeader}>
                    <Text> Game is {result} </Text>
                </View>

                <View style={styles.gameInfo}>
                    <Text> {state}</Text>
                </View>
                <View style={styles.iconContainer}>
                    <View style={styles.icon}>
                        <TouchableOpacity onPress={()=>assignMove("PAPER")}>
                            <FontAwesome5 name="hand-paper" size={50} color="black" />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.icon}>
                        <TouchableOpacity onPress={()=>assignMove("ROCK")}>
                            <FontAwesome5  name="hand-rock" size={50} color="black" />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.icon}>
                        <TouchableOpacity onPress={()=>assignMove("SCISSORS")}>
                            <FontAwesome5  name="hand-scissors" size={50} color="black" />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.result}>
                    <Text> Result:{result}</Text>
                </View>
                <View style={styles.info}>
                    <View style={styles.yourInfo}>
                        <Text>Name:{name}</Text>
                        <Text>Move:{move}</Text>
                    </View>
                    <View style={styles.opponentInfo}>
                        <Text> Opponent :{opponentName}</Text>
                        <Text> Opp. Move:{opponentMove}</Text>
                    </View>

                </View>

            </Card>


            <Button color='maroon' title="Go TO Home" onPress={() => navigation.navigate('Home')} />

        </View>
    );
}

const styles = StyleSheet.create({
    container:{

        justifyContent:'center',
        alignItems:'center',

    },
    info:{
      flexDirection:"row",
        backgroundColor:'grey',
        width: '100%',
        justifyContent:'space-between',
    },
    iconContainer:{
        flexDirection:'row',
        justifyContent: 'space-around'
    },
    icon:{
        marginTop:10,
        backgroundColor: '#94f0f1',
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        overflow: "hidden",
        borderWidth: 3,
        borderColor: "blue",
        textAlign:'center',
        justifyContent:'center',
        alignItems:'center',

    },
    cardHeader:{
        width: '100%',
        height: 50,
        backgroundColor: 'rgb(223, 31, 127)',
        justifyContent:'center',
        alignItems:'center',

    },
    yourInfo:{
        width: '45%',
        height: 50,
        backgroundColor: '#94f0f1',
        justifyContent:'center',
        alignItems:'center',
        margin:5


    },
    opponentInfo:{
        width: '45%',
        height: 50,
        backgroundColor: 'green',
        justifyContent:'center',
        alignItems:'center',
        margin:5



    },
    status:{
        width: '100%',
        height: 50,
        backgroundColor: '#ffdc6a',
        justifyContent:'center',
        alignItems:'center',

    },
    gameInfo:{
        width: '100%',
        height: 50,
        backgroundColor: '#ffdc6a',
        justifyContent:'center',
        alignItems:'center',
        fontWeight:'bold',
        fontSize:30

    },
    result:{
        backgroundColor: '#ffdc6a',
        height:30,
        justifyContent:'center',
        alignItems:'center',
        fontWeight:'bold',
        fontSize:30
    },
    scores:{
        flexDirection: 'row',
        justifyContent: 'space-around'

    },
    score:{
        fontSize: 40,
    },
    name:{
        fontSize: 30,
    }
});

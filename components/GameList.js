import React, {useContext, useEffect, useState} from 'react';
import {FlatList,StyleSheet, Text, View, Image, SafeAreaView, TouchableOpacity, Button,ActivityIndicator} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Card from "./Card";
import Game from "./Game";
import Api from "../api/Api";
import TokenContext from "../context/TokenContext";
import GameCard from "./GameCard";



export default function GameList({ navigation }) {
    const token = useContext(TokenContext);
    const [allGames,setAllGames]=useState([]);
    const [isLoading,setLoading]=useState(true);
    useEffect(()=>{
        Api.allGames(token).then(res=>{
            setAllGames(res);
        }).catch((error) => console.error(error))
            .finally(() => setLoading(false));
    },[])
    return (
        <View style={styles.container}>
            {isLoading ? <ActivityIndicator/> : (
                <FlatList
                    data={allGames}
                    keyExtractor={item  => item.id}
                    renderItem={({ item }) => (
                        <GameCard item={item} navigation={navigation}/>
                    )}
                />
            )}

        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fffacd',

    },
});

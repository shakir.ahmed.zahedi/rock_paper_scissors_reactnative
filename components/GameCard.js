
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View, Image, SafeAreaView, TouchableOpacity, Button} from 'react-native';
import Card from "./Card";
import Api from "../api/Api";
import TokenContext from "../context/TokenContext";


export default function GameCard({item,navigation}) {
    const token = useContext(TokenContext);
    const joinAGame=()=>{
        Api.joinGame(token,item.id).then(res=>{
            navigation.navigate('Game');
        })
    }
    return (

            <Card >
                <View style={styles.cardBody}>
                    <Text style={styles.text}>{item.name}</Text>
                    <Text>{item.id}</Text>
                    <View style={styles.button}>
                        <Button title="JOIN" onPress={joinAGame} />
                    </View>
                </View>


            </Card>



    );
}

const styles = StyleSheet.create({
    card: {
        flex:1,
       // flexDirection: 'row',
        //paddingTop:10,
        backgroundColor:'rgb(223, 31, 127)',
        justifyContent: 'center',
        alignItems:'center',
      //  width: '100%',
        height: 80,
        margin:10
    },
    text: {
        fontWeight:'bold',


    },
    cardBody:{
        flex:2,
        justifyContent: 'center',
        alignItems:'center',
       // backgroundColor:'rgb(223, 31, 127)',
    },
    button:{
        fontWeight: 'bold',
        justifyContent: 'center',
        marginTop:10,
        width:100,
        borderWidth: .5,
        borderRadius: 6,
        borderColor:'#ddd',
        color:'black',
        elevation: 3,
        backgroundColor: '#ffdc6a',
        shadowOffset: { width: 1, height: 1 },
        shadowColor: '#333',
        shadowOpacity: 0.3,
        shadowRadius: 2,


    }



});

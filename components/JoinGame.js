import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, TextInput, Button} from 'react-native';
import Card from "./Card";
import Header from "./Header";
import Api from "../api/Api";
import TokenContext from "../context/TokenContext";
import GameCard from "./GameCard";

const joinGame= ({navigation})=>{
    const [name, setName] = React.useState("");
    const [text, onChangeText] = React.useState("");
    const [gameID, setGameId] = React.useState("");
    const token = useContext(TokenContext);
    const joinGame=()=>{
        Api.setName(token,text)
            .then(res=>{
                console.log("Name:"+res);
            })
            .catch(err=> console.log(err));
        navigation.navigate('GameList', {
            id: gameID,
            otherParam: 'anything you want here',
        });
    }


    return(
        <View >
            <View>
                <Header  navigation={navigation}/>
            </View>
            <View style={styles.cardView}>
                <Card >
                    <View style={styles.cardHeader}>
                        <Text style={styles.text}> Set Name</Text>
                    </View>
                    <View>
                        <TextInput
                            style={styles.input}
                            placeholder='Player Name'
                            onChangeText={onChangeText}
                            value={text}
                        />
                        <View  style={styles.buttonGroup}>
                            <View style={styles.buttonView}>
                                <Button color='black' title="OK" onPress={joinGame}   />
                            </View>
                            <View style={styles.buttonView}>
                                <Button color='maroon' title="Cancel"  onPress={() => navigation.navigate('Home')} />
                            </View>






                        </View>
                    </View>
                </Card>
            </View>


        </View>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',


    },
    buttonView:{

        width:100,
        borderWidth: .5,
        borderRadius: 6,
        borderColor:'#ddd',
        color:'black',
        elevation: 3,
        backgroundColor: '#6cd7f5',
        shadowOffset: { width: 1, height: 1 },
        shadowColor: '#333',
        shadowOpacity: 0.3,
        shadowRadius: 2,

    },
    text:{
        fontWeight:'bold',
        fontSize: 30

    },
    cardView:{
        marginTop:50,
        marginHorizontal:30
    },
    card:{
        paddingTop: 100,
        padding: 20
    },
    cardHeader:{
        width: '100%',
        height: 50,
        backgroundColor: 'red',
        justifyContent:'center',
        alignItems:'center',

    },
    buttonGroup:{
        flexDirection: 'row',
        paddingTop:20,
        margin:5,
        justifyContent: 'space-around',
    },
    input:{
        borderWidth: 1,
        borderColor: '#ddd',
        padding: 10,
        fontSize: 18,
        borderRadius: 6,
    }

});
export default joinGame;

import React, {useEffect, useState, useContext} from 'react';
import { StyleSheet, Text, View,TouchableOpacity,TextInput } from 'react-native';
import Api from "./Api";
import * as SecureStore from "expo-secure-store";
import TokenContext from "../context/TokenContext";




export default function Try() {
    const token = useContext(TokenContext);
    //const [token,setToken]= useState('');
    const [text, onChangeText] = React.useState("");
    const [ami, setAmi] = React.useState("");
   /* const getToken=()=> {
        fetch('http://192.168.0.7:8086/auth/token')
            .then((response) => response.text())

            .then(text => {
                setToken(text);
                console.log(text) ;
            })
            .catch((error) => console.error(error))

    }*/
    const setName= ()=>{
       /* const response= await fetch('http://192.168.0.7/users/name',
            {
                method: "POST",
                body: JSON.stringify({name: text,userName:""}),
                headers: {
                    token: token,
                    'Accept': 'application/json',
                    'Access-Control-Allow-Origin': '*',

                    'Content-Type': 'application/json',
                }
            });
        const result = await response.text();
        return result;
       // Api.setName(text)*/
      return fetch('http://192.168.0.7:8086/user/name', {
            method: "POST",
            body: JSON.stringify({name:text,userName:""}),
            headers: {
                token: token,
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*',

                'Content-Type': 'application/json',
            }
        })
            .then((response) =>
                response.text()
               // setAmi(data);
               // console.log(data);

            )
          .then(res => {
                console.log(res);
                setAmi(res)
            console.log(res)})

        .catch((error) => console.error(error))
    }

    const startGame=()=>{
       /* fetch('http://192.168.0.7:8086/games/start', {headers: {'token': token}})
            .then(response => response.json())*/
        Api.newGame()
            .then(res=> console.log(res))
            .catch((error) => console.error(error))


    }
    const setMove=(sign)=>{
        fetch('http://192.168.0.7:8086/games/move/' +sign, {headers: {'token': token}})
            .then(response => response.json())
            .then(res=> console.log(res))
            .catch((error) => console.error(error))


    }
    const getGameStatus=()=>{
        fetch('http://192.168.0.7:8086/games/status', {headers: {'token': token}})
            .then(response => response.json())
            .then(res=> console.log(res))
            .catch((error) => console.error(error))


    }


    /* fetch('http://localhost:8086/auth/token')
         .then((response) => {
            let res= response.text();
            console.log(res);


         }).then((res)=>{
          console.log(res) ;
     })
         .catch((error) => console.error(error))

 }*/

    return(
        <View style={styles.container}>
            <Text> Your Token:</Text>
            <Text> {token}</Text>
            <Text> Your Name:</Text>
            <Text> {text}</Text>
            <Text> {ami}</Text>
            <TextInput
                style={styles.input}
                onChangeText={onChangeText}
                value={text}
            />

            <TouchableOpacity onPress={setName}>
                <Text>SetName </Text>

            </TouchableOpacity>
            <TouchableOpacity onPress={startGame}>
                <Text>StartGame </Text>

            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setMove("ROCK")}}>
                <Text>Move </Text>

            </TouchableOpacity>
            <TouchableOpacity onPress={getGameStatus}>
                <Text>Game Status </Text>

            </TouchableOpacity>
        </View>
    );

}
/*
//const Drawer = createDrawerNavigator();

return (
    <NavigationContainer>
      <Drawer.Navigator >
        <Drawer.Screen name="Home"
                       component={Home}

                         />
        <Drawer.Screen name="Join Game" component={JoinStack}
                       options={({ navigation })=>
                           ({
                             headerTitle: () => <Header navigation={navigation}/>
                           })}
        />
        <Drawer.Screen name="Start Game" component={StartStack} />
      </Drawer.Navigator>
    </NavigationContainer>
    /*<NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} />

        <Stack.Screen name="Form" component={Form} />
        <Stack.Screen name="GameList" component={GameList} />

        <Stack.Screen name="Game" component={Game} />
      </Stack.Navigator>
    </NavigationContainer>*/
// );
//}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop:30
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
    },
    home:{
        margin: 10,
        width: '100%',
        height: 600,
        padding:20
    },
    cardHeader:{
        width: '100%',
        height: 50,
        backgroundColor: 'red',
        justifyContent:'center',
        alignItems:'center',



    }
});

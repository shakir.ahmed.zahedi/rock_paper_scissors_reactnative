import React from 'react';
import {StyleSheet, Text, View,TextInput,Button} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Formik} from "formik";
import Card from "./Card";
import Game from "./Game";
import Home from "./Home";
import GameList from "./GameList";


export default function Form({ navigation}) {
    const creatNewGame=(PName)=> {
       console.log(PName);
        const playerName = PName;
        const exitPlayerName = async () => {
            try {
              const value= await AsyncStorage.getItem("playerName");
                console.log("Value:"+value);

            } catch (e) {
               console.log("Error-18");
            }
        }

        if (exitPlayerName === "" || exitPlayerName === null || exitPlayerName === "null") {
            const storeData = async (res) => {
                try {
                    const value=await AsyncStorage.setItem('playerName', playerName)
                } catch (e) {
                    console.log("Error-28");
                }
            }
        }
        else {
            const storeData = async (res) => {
                try {
                    await AsyncStorage.setItem('playerName', exitPlayerName)
                } catch (e) {
                    console.log("Error-38")
                }
            }
        }
        const currentName = async () => {
            try {
                const value=  await AsyncStorage.getItem("playerName");
                console.log("Error-48");
                console.log(value);
                checkTokenToCreateNewGame(value);
            } catch (e) {
                console.log("Error-48");
            }
        }





    }

    const checkTokenToCreateNewGame=(name)=> {
        const token = async () => {
            try {
                await AsyncStorage.getItem('myToken')
            } catch (e) {
                // saving error
            }
        }
        console.log("1st:"+token);
        if (token === "" || token === null || token === "null") {
           Api.getToken().then(res => {
               const storeData = async (res) => {
                   try {
                       await AsyncStorage.setItem('myToken', res);
                       console.log("2nd:"+token);
                   } catch (e) {
                       // saving error
                   }
               }

              Api.setName(res, { 'name': name }).then(data => {
                    console.log(data);
                });
               Api.getStartGame(res).then(data => {
                    console.log(data);

                }).finally(() => {
                   navigation.navigate('Game');
                });
            });

        }
        else {
           Api.getStartGame(token).then(data => {
                console.log(data);
            }).finally(() => {
                navigation.navigate('Game');
            });
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.card}>
            <Card >
                <View style={styles.cardHeader}>
                    <Text> Set Name</Text>
                </View>
                <View>
                    <Formik
                        initialValues={{ name:'' }}
                        onSubmit={(values) => {
                            console.log(values.name);
                            creatNewGame(values.name);
                         // navigation.navigate('GameList');
                        }}
                    >
                        {props => (
                            <View>
                                <TextInput
                                    style={styles.input}
                                    placeholder='Player Name'
                                    onChangeText={props.handleChange('name')}
                                    value={props.values.name}
                                />
                                <View  style={styles.buttonGroup}>
                                    <Button color='maroon' title="OK" onPress={props.handleSubmit} />
                                    <Button color='maroon' title="Cancel"  onPress={() => navigation.navigate('Home')} />

                                </View>

                            </View>
                        )}
                    </Formik>
                </View>
            </Card>
            </View>
        </View>

    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',


    },
    card:{
      paddingTop: 100,
        padding: 20
    },
    cardHeader:{
        width: '100%',
        height: 50,
        backgroundColor: 'red',
        justifyContent:'center',
        alignItems:'center',

    },
    buttonGroup:{
        flexDirection: 'row',
        paddingTop:20,
        justifyContent: 'space-around',
        height: 100,

    },
    input:{
        borderWidth: 1,
        borderColor: '#ddd',
        padding: 10,
        fontSize: 18,
        borderRadius: 6,
        height:100
    }

});

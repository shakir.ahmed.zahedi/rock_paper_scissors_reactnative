import React, {Component, useContext} from "react";
import * as SecureStore from 'expo-secure-store';
import {StyleSheet, Text, View, TouchableOpacity, TextInput, Button} from 'react-native';
import TokenContext from "../context/TokenContext";

class Api {

    startGame=(token)=>{
         return fetch('http://192.168.0.7:8086/games/start', {headers: {'token': token}})
            .then(response => response.json())
    }
    joinGame= (token,gameId) => {
        return fetch('http://192.168.0.7:8086/games/join/' + gameId, {headers: {'token': token}})
            .then(response => response.json())
    }
    allGames= (token) => {
        return fetch('http://192.168.0.7:8086/games', {headers: {'token': token}})
            .then(response => response.json())
    }
    makeMove=(token,move) => {
    return fetch('http://192.168.0.7:8086/games/move/' + move, {headers: {'token': token}})
           .then(response => response.json())
}
    getGameStatus=(token) => {
    return fetch('http://192.168.0.7:8086/games/status', {headers: {'token': token}})
            .then(response => response.json())
    }


     setName= (token,name) => {

        return fetch('http://192.168.0.7:8086/user/name', {
            method: "POST",
            body: JSON.stringify({name:name,userName:""}),
            headers: {
                token:token,
                'Accept': 'application/json',
                'Access-Control-Allow-Origin': '*',

                'Content-Type': 'application/json',
            }
        })
            .then((response) =>
                response.text()

                // setAmi(data);
                 //console.log("myData"+response.text());}

            )
    };

    return;
}
export default  new Api();









